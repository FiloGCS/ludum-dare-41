﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public Image mainMenuCurtain;

	void Start () {
		if (mainMenuCurtain != null) {
			mainMenuCurtain.CrossFadeAlpha(0f, 1f, false);
		}
	}

    public void Exit() {
        Application.Quit();
    }
    public void StartGame() {
		StartCoroutine ("AnimatedStart");
    }

	public IEnumerator AnimatedStart(){
		mainMenuCurtain.CrossFadeAlpha(1f, 1f, false);
		yield return new WaitForSeconds (1f);
		SceneManager.LoadScene("Scene1");
	}
} 
