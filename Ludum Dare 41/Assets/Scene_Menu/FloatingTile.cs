﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingTile : MonoBehaviour {

	public float AMPLITUDE;
	public float SPEED;
	public float timeOffset;
	private Vector3 referencePosition;
	// Use this for initialization
	void Start () {
		referencePosition = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = referencePosition + Vector3.up * AMPLITUDE * Mathf.Cos((Time.time+timeOffset)*SPEED);
	}
}
