﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PieceType { Pawn, Horse, King, Bishop, Rook, Queen};
