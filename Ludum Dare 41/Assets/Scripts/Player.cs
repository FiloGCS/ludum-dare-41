﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public int score = 0;
    public int moves = 10;
    public float COOLDOWN = 0.5f;

    public GameController gc;
    public float TimeForNextMove = 0;

    private List<int[]> potentialMoves;
    private GameObject currentTile;
    private Tile touchedTile = null;

    public void Init(GameController gc, GameObject tile) {
        this.gc = gc;
        this.currentTile = tile;
        currentTile.GetComponent<Tile>().SetIsOccupiedByPlayer(true);
        UpdatePossibleMoves();
    }

    void Update() {

        UpdatePossibleMoves();
        //Update player Cooldown
        //transform.GetChild(0).GetComponent<Renderer>().material.SetFloat("_percent", TimeForNextMove -Time.time);


        //Check for mouse click
        if (Input.GetMouseButtonDown(0) && TimeForNextMove<=Time.time && !gc.bIsPaused) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit)) {
                //Is it a tile?
                touchedTile = hit.transform.GetComponent<Tile>();
                if (touchedTile) {
                    if (touchedTile.bIsPossibleForPlayer) { //Is it a possible move?
                        if (touchedTile.enemyOccupier != null) { //If the tile is occupied by an enemy
                            //Talk with him!
                            gc.StartConversation(this.gameObject, touchedTile.GetComponent<Tile>().enemyOccupier);
                        } else { //If the tile is free...
                            CommitMove();
                        }
                    } else {
                        touchedTile = null;
                    }
                }
            }
        }
    }

    public void CommitMove() {
        if (touchedTile != null) {
            print("COMMITING: " + touchedTile.x + "," + touchedTile.y);
            //Change the current Tile to the new one
            currentTile.GetComponent<Tile>().SetIsOccupiedByPlayer(false);
            currentTile = touchedTile.gameObject;
            //Move the piece
            GetComponent<Piece>().SetPosition(touchedTile.x, touchedTile.y);
            UpdatePossibleMoves();
            currentTile.GetComponent<Tile>().SetIsOccupiedByPlayer(true);
            //Start Cooldown
            TimeForNextMove = Time.time + COOLDOWN;
        }
    }

    //Update the highlighted tiles
    public void UpdatePossibleMoves() {
        //Clear previous possible moves if any
        if (potentialMoves!=null) {
            foreach (int[] move in potentialMoves) {
                gc.tiles[move[0], move[1]].GetComponent<Tile>().SetIsPossibleForPlayer(false);
            }
        }
        //Get new possible player moves
        potentialMoves = gc.GetPossibleMoves(GetComponent<Piece>());
        //Highlight them
        foreach (int[] move in potentialMoves) {
            gc.tiles[move[0], move[1]].GetComponent<Tile>().SetIsPossibleForPlayer(true);
        }
    }
}
