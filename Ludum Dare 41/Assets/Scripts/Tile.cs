﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {
    private Color color;
    public  int x;
    public  int y;

    public bool bIsPossibleForPlayer = false;
    public bool bIsOccupiedByPlayer = false;
    public GameObject enemyOccupier = null;
    public GameController gc;

    void Update() {
        ///TODO Hack
        if (bIsPossibleForPlayer) {
            float alpha = (0.5f - (gc.player.TimeForNextMove - Time.time))*2f;
            transform.GetChild(0).GetComponent<Renderer>().material.SetFloat("_Alpha", alpha);
        } else {
            transform.GetChild(0).GetComponent<Renderer>().material.SetFloat("_Alpha", 0.0f);
        }
    }

    public void Init(GameController gc) {
        this.gc = gc;
    }

    public void SetPosition(int x, int y) {
        this.x = x;
        this.y = y;
        //Set the color of the tile depending on its position
        this.color = ((x + y) % 2 == 0) ?
            Color.black :
            Color.white;
        //And the material of the gameobject
        if(this.color == Color.black) {
            this.GetComponent<Renderer>().material = (Material)Resources.Load("Materials/M_Tile_Black");
        } else {
            this.GetComponent<Renderer>().material = (Material)Resources.Load("Materials/M_Tile_White");
        }
    }
    
    public void SetIsPossibleForPlayer(bool isPossible) {
        bIsPossibleForPlayer = isPossible;
    }

    public void SetIsOccupiedByPlayer(bool isOcuppied) {
        bIsOccupiedByPlayer = isOcuppied;
    }

    public void SetIsOccupiedByEnemy(GameObject occupier) {
        enemyOccupier = occupier;
    }
}
