﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConversationController : MonoBehaviour {
    
    public ConversationPanel cPanel;

    private List<ConversationState> bishop0 = new List<ConversationState>();
    private List<ConversationState> bishop1 = new List<ConversationState>();
    private List<ConversationState> horse0 = new List<ConversationState>();
    private List<ConversationState> king0 = new List<ConversationState>();
    private List<ConversationState> king1 = new List<ConversationState>();
    private List<ConversationState> story0 = new List<ConversationState>();
    private List<ConversationState> story1 = new List<ConversationState>();
    private List<ConversationState> story2 = new List<ConversationState>();
    private List<ConversationState> pawn0 = new List<ConversationState>();
    private List<ConversationState> rook0 = new List<ConversationState>();
    private ConversationState currentState;
    

    void Awake() {
        //Load Conversations
        InitConversations();
    }

    public void StartStory(int story) {
        switch (story) {
            case 0:
                SetConversationState(story0[0]);
                break;
            case 1:
                SetConversationState(story1[0]);
                break;
            case 2:
                SetConversationState(story2[0]);
                break;
            default:
                break;
        }
    }

    public void StartConversation(GameObject talker, GameObject listener) {
        //PLAYER CONVERSATIONS
        if (listener.GetComponent<Enemy>() != null) {
            switch (listener.GetComponent<Piece>().type) {
                //When the players talks to a bishop
                case PieceType.Bishop:
                    switch (listener.GetComponent<Enemy>().conversationNumber) {
                        case 0:
                            SetConversationState(bishop0[0]);
                            break;
                        case 1:
                            SetConversationState(bishop1[0]);
                            break;
                        default:
                            SetConversationState(bishop0[0]);
                            break;
                    }
                    break;
                //When the players talks to a king
                case PieceType.King:
                    switch (listener.GetComponent<Enemy>().conversationNumber) {
                        case 0:
                            SetConversationState(king0[0]);
                            break;
                        default:
                            SetConversationState(king1[0]);
                            break;
                    }
                    break;
                //When the players talks to a horse
                case PieceType.Horse:
                    SetConversationState(horse0[0]);
                    break;
                case PieceType.Pawn:
                    SetConversationState(pawn0[0]);
                    break;
                case PieceType.Rook:
                    SetConversationState(rook0[0]);
                    break;
                default:
                    SetConversationState(bishop0[0]);
                    break;
            }
        }
    }

    public void ChooseAnswer(int i) {
        SetConversationState(currentState.cChildren[i]);
    }

    public void EndConversation() {
        cPanel.Hide();
        GetComponent<GameController>().EndConversation("");
    }

    private void SetConversationState(ConversationState cs) {
        currentState = cs;
        if (currentState != null) {
            //Imagination Change?
            if (cs.cImaginationChange != 0) {
                GetComponent<GameController>().AddImagination(cs.cImaginationChange);
            }
            if (cs.cAction!=null) {
                switch (cs.cAction) {
                    case "fadein":
                        cPanel.SetCurtain(false);
                        break;
                    case "fadeout":
                        cPanel.SetCurtain(true);
                        break;
                    case "hurt":
                        GetComponent<GameController>().AddImagination(-GetComponent<GameController>().imagination/3f);
                        break;
                    case "end":
                        cPanel.ShowEndPanel();
                        break;
                    case "load_level2":
						GetComponent<GameController>().EndConversation("load_level2");
                        break;
					case "load_level3":
						GetComponent<GameController>().EndConversation("load_level3");
                        break;

                    default:
                        break;
                }
            }
                //Sound
                if (cs.cSound != null) {
                AudioClip clip = Resources.Load<AudioClip>("Sounds/"+cs.cSound);
                this.GetComponent<AudioSource>().clip = clip;
                this.GetComponent<AudioSource>().Play();
            }
            //Avatar
            if (cs.cAvatar != null) {
                cPanel.SetAvatar(cs.cAvatar);
            }
            //Name
            if (cs.cName != null) {
                cPanel.SetName(cs.cName);
            }
            //Dialogue
            if (cs.cDialogue != null) {
                cPanel.SetDialogue(cs.cDialogue);
            }
            //Answers
            for (int i = 0; i < 3; i++) {
                string thisAnswer = (cs.cAnswers[i]) ? cs.cAnswersText[i] : null;
                cPanel.SetAnswer(i, thisAnswer);
            }
            //Show Panel
            cPanel.Show();
        } else {
            EndConversation();
        }
    }

    private void InitConversations() {
        //STATES
        ConversationState cs;
        
        //BISHOP 0
        //0
        cs = new ConversationState("T_Bishop_normal", "A common bishop", "What is happening?! Nobody is following their turn!",
            new bool[] {true,true,false}, new string[] { "Do you want to be my friend?","I can imagine you out of existence",""});
        bishop0.Add(cs);
        //1
        cs = new ConversationState("T_Bishop_normal", "A common bishop", "Why would I?",
            new bool[] { true, true, false }, new string[] { "My friends can fly", "I don't know...", "" });
        bishop0.Add(cs);
        //2
        cs = new ConversationState("T_Bishop_normal", "A common bishop", "Sign me in",
            new bool[] { true, false, false }, new string[] { "Great!", "", "" });
        cs.cImaginationChange = 10.0f;
        bishop0.Add(cs);
        //3
        cs = new ConversationState("T_Lily", "Lily", "That's not how you play",
            new bool[] { true, false, false }, new string[] { "Sorry...", "", "" });
        cs.cImaginationChange = -5.0f;
        bishop0.Add(cs);
        //4
		cs = new ConversationState("T_Imagination", "My Imagination", "You have to imagine harder dude!",
            new bool[] { true, false, false }, new string[] { "Ok", "", "" });
        bishop0.Add(cs);
        //5
        cs = new ConversationState("T_Bishop_normal", "A common bishop", "Stop! You have to follow the rules!",
            new bool[] { true, false, false }, new string[] { "Maybe you do", "", "" });
        cs.cImaginationChange = 5.0f;
        bishop0.Add(cs);
        //Bishop 0 Connections
        bishop0[0].cChildren[0] = bishop0[1];
        bishop0[0].cChildren[1] = bishop0[5];
        bishop0[1].cChildren[0] = bishop0[2];
        bishop0[1].cChildren[1] = bishop0[3];
        bishop0[3].cChildren[0] = bishop0[4];

        //BISHOP 1
        //0
        cs = new ConversationState("T_Bishop_normal", "A bishop with a foil hat", "Imagination is the biggest lie the government has told",
            new bool[] { true, true, true }, new string[] { "You're nuts!", "Maybe you're right", "Oh the irony..." });
        bishop1.Add(cs);
        //1
        cs = new ConversationState("T_Bishop_normal", "A bishop with a foil hat", "You gotta look into it!",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        cs.cImaginationChange = 5f;
        bishop1.Add(cs);
        //2
		cs = new ConversationState("T_Imagination", "My Imagination", "No!, Stay strong!",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        cs.cImaginationChange = -5f;
        bishop1.Add(cs);

        bishop1[0].cChildren[0] = bishop1[1];
        bishop1[0].cChildren[1] = bishop1[2];
        bishop1[0].cChildren[2] = bishop1[1];

        //ROOK 0
        //0
        cs = new ConversationState("T_Rook_normal", "A regular Rook", "Woof",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        rook0.Add(cs);
        //1
        cs = new ConversationState("T_Rook_normal", "Wait no, it's a dog", "Woof Woof",
            new bool[] { true, true, false }, new string[] { "Good Boy", "*pat*", "" });
        rook0.Add(cs);
        //2
        cs = new ConversationState("T_Rook_normal", "A Dog", "*Goes on woofing*",
            new bool[] { true, false, false }, new string[] { "Cool...", "", "" });
        rook0.Add(cs);

        rook0[0].cChildren[0] = rook0[1];
        rook0[1].cChildren[0] = rook0[2];
        rook0[1].cChildren[1] = rook0[2];

        //PAWN 1
        //0
        cs = new ConversationState("T_Pawn_normal", "A pun", "I'm a pun",
            new bool[] { true, true, false }, new string[] { "Oh, I get it...", "Not funny.", "" });
        pawn0.Add(cs);
        //1
        cs = new ConversationState("T_Pawn_normal", "A pun", "Yeah...",
            new bool[] { true, true, false }, new string[] { "...", "It's a very intelligent word play ", "" });
        pawn0.Add(cs);
        //2
        cs = new ConversationState("T_Pawn_normal", "A bad pun", "Oh...",
            new bool[] { true, true, false }, new string[] { "...", "I like your hairdo though", "" });
        pawn0.Add(cs);
        //3
		cs = new ConversationState("T_Imagination", "My Imagination", "Awkward...",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        cs.cImaginationChange = -5;
        pawn0.Add(cs);
        //4
        cs = new ConversationState("T_Pawn_normal", "A happy pun", "Really? Thanks!",
            new bool[] { true, false, false }, new string[] { "*smile awkwardly*", "", "" });
        cs.cImaginationChange = 10;
        pawn0.Add(cs);

        pawn0[0].cChildren[0] = pawn0[1];
        pawn0[0].cChildren[1] = pawn0[2];
        pawn0[1].cChildren[0] = pawn0[3];
        pawn0[1].cChildren[1] = pawn0[4];
        pawn0[2].cChildren[0] = pawn0[3];
        pawn0[2].cChildren[1] = pawn0[4];

        //Horse 0
        //0
        cs = new ConversationState("T_Horse_normal", "Handsome horse", "Oh, hi Sam.",
            new bool[] { true, true, false }, new string[] { "Wow! You're a handsome guy!", "It's a shame I have to destroy such beautiful stallion.", "" });
        horse0.Add(cs);
        //1
        cs = new ConversationState("T_Horse_normal", "Handsome horse", "Thank you, but I'm a mare!",
            new bool[] { true, true, false }, new string[] { "Oh, still...","A what?", "" });
        horse0.Add(cs);
        //2
        cs = new ConversationState("T_Horse_normal", "Handsome mare", "A female horse, how can you not know that?",
            new bool[] { true, true, false }, new string[] { "I'm ten.", "Female? what is that?", "" });
        horse0.Add(cs);
        //3
        cs = new ConversationState("T_Horse_normal", "Handsome mare", "Even then...",
            new bool[] { true, true, false }, new string[] { "I'm from the city.", "My mom says I'm special, too.", "" });
        horse0.Add(cs);
        //4
        cs = new ConversationState("T_Horse_normal", "Handsome mare", "Dude...",
            new bool[] { true, true, true }, new string[] {"*Imagine this so called 'mere' in a happy place*", "*Imagine this 'fern-ale' horse into oblivion*", "*Imagine Dragons*" });
        horse0.Add(cs);
        //5
        cs = new ConversationState("T_Horse_normal", "Handsome mare", "What did you want?",
            new bool[] { true, true, true }, new string[] { "*Imagine the mere running through fields far away*", "*Imagine the mere suffering for a thousand eternities*", "'Imagine' is an overrated song"});
        horse0.Add(cs);
        //6
        cs = new ConversationState("T_Horse_normal", "Handsome mare", "What?! No!",
            new bool[] { true, false, false }, new string[] { "*laugh in spanish*", "", "" });
        horse0.Add(cs);
        //7
        cs = new ConversationState("T_Horse_normal", "Handsome mare", "*smiles and trots away*",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        horse0.Add(cs);

        horse0[0].cChildren[0] = horse0[1];
        horse0[0].cChildren[1] = horse0[1];
        horse0[1].cChildren[0] = horse0[5];
        horse0[1].cChildren[1] = horse0[2];
        horse0[2].cChildren[0] = horse0[3];
        horse0[2].cChildren[1] = horse0[4];
        horse0[3].cChildren[0] = horse0[5];
        horse0[3].cChildren[1] = horse0[5];
        horse0[4].cChildren[0] = horse0[7];
        horse0[4].cChildren[1] = horse0[6];
        horse0[4].cChildren[2] = horse0[7];
        horse0[5].cChildren[0] = horse0[7];
        horse0[5].cChildren[1] = horse0[6];
        horse0[5].cChildren[2] = horse0[6];


        //King 0
        //0
        cs = new ConversationState("T_King_normal", "Black King", "You're making a huge mistake. This is not how chess works.",
            new bool[] { true, false, false }, new string[] { "I'm unstoppable", "", "" });
        king0.Add(cs);
        //1
        cs = new ConversationState("T_King_normal", "Black King", "Maybe now you are.",
            new bool[] { true, false, false }, new string[] { "What does that mean?", "...", "" });
        king0.Add(cs);
        //2
        cs = new ConversationState("T_King_normal", "Black King", "What about real life?",
            new bool[] { true, true, false }, new string[] { "What about it?", "I don't want to play anymore", "" });
        king0.Add(cs);
        //3
        cs = new ConversationState("T_King_normal", "Black King", "What about me?",
            new bool[] { true, false, false }, new string[] { "I could destroy you like any other piece.", "", "" });
        king0.Add(cs);
        //4
        cs = new ConversationState("T_King_normal", "Black King", "Do you want to try? Do you want to fight at school tomorrow?",
            new bool[] { true, false, false }, new string[] { "What?", "", "" });
        king0.Add(cs);
        //5
        cs = new ConversationState("T_King_normal", "Black King", "Who am I, Sam?",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        cs.cAction = "fadeout";
        king0.Add(cs);
        //6
        cs = new ConversationState("T_King_normal", "Black King", "...",
            new bool[] { true, false, false }, new string[] { "Buck?", "", "" });
        king0.Add(cs);
        //7
        cs = new ConversationState("T_King_normal", "Buck from 6th Grade", "That's right Sam, I will kick your ass again",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        king0.Add(cs);
        //8
        cs = new ConversationState("T_King_normal", "Buck from 6th Grade", "*punch*",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        cs.cAction = "hurt";
        king0.Add(cs);
        //9
		cs = new ConversationState("T_Imagination", "My Imagination", "What's happening?! I'm losing strength!",
            new bool[] { true, false, false }, new string[] { "It's Buck!", "", "" });
        cs.cAction = "hurt";
        king0.Add(cs);
        //10
        cs = new ConversationState("T_King_normal", "Buck from 6th Grade", "What's up pussy? You look scared?",
            new bool[] { true, true, false }, new string[] { "*try to get up*", "*try to kick him*", "" });
        king0.Add(cs);
        //11
        cs = new ConversationState("T_King_normal", "Buck from 6th Grade", "*punch*",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        cs.cAction = "hurt";
        king0.Add(cs);
        //12
		cs = new ConversationState("T_Imagination", "My Imagination", "I'm sorry Sam, I can't do this",
            new bool[] { true, false, false }, new string[] { "*cry*", "", "" });
        king0.Add(cs);
        //13
        cs = new ConversationState("T_Question", "???", "Hey!",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        king0.Add(cs);
        //14
        cs = new ConversationState("T_King_normal", "Buck from 6th Grade", "Huh?",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        king0.Add(cs);
        //15
        cs = new ConversationState("T_Question", "???", "You like messing with my brother, fool?",
            new bool[] { true, false, false }, new string[] { "Lily?", "", "" });
        king0.Add(cs);
        //16
        cs = new ConversationState("T_Lily", "Lily from 9th grade", "Come here dummy, I got something for you!",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        king0.Add(cs);
        //17
        cs = new ConversationState("T_Lily", "Lily from 9th grade", "*Pulls Buck's pants down*",
            new bool[] { true, true, false }, new string[] { "*laugh*", "*feel bad for Buck*", "" });
        cs.cImaginationChange = 50f;
        king0.Add(cs);
        //18
        cs = new ConversationState("T_Lily", "Lily from 9th grade", "Let's go kiddo, what do you wanna do?",
            new bool[] { true, true, true }, new string[] { "Let's play again", "Let's play some LD41 entries", "Oooh! pun... pawn.... I get it now..." });
        king0.Add(cs);
        //19
        cs = new ConversationState("T_Imagination", "My Imagination", "You have got to be kidding me...",
            new bool[] { true, false, false }, new string[] { "*The End*", "", "" });
        cs.cAction = "end";
        king0.Add(cs);
        //20
        cs = new ConversationState("T_Imagination", "My Imagination", "Wow, so meta...",
            new bool[] { true, false, false }, new string[] { "*The End*", "", "" });
        cs.cAction = "end";
        king0.Add(cs);
        //21
        cs = new ConversationState("T_Imagination", "My Imagination", "Still cringy...",
            new bool[] { true, false, false }, new string[] { "*The End*", "", "" });
        cs.cAction = "end";
        king0.Add(cs);

        king0[0].cChildren[0] = king0[1];
        king0[1].cChildren[0] = king0[2];
        king0[2].cChildren[0] = king0[3];
        king0[2].cChildren[1] = king0[3];
        king0[3].cChildren[0] = king0[4];
        king0[4].cChildren[0] = king0[5];
        king0[5].cChildren[0] = king0[6];
        king0[6].cChildren[0] = king0[7];
        king0[7].cChildren[0] = king0[8];
        king0[8].cChildren[0] = king0[9];
        king0[9].cChildren[0] = king0[10];
        king0[10].cChildren[0] = king0[11];
        king0[10].cChildren[1] = king0[11];
        king0[11].cChildren[0] = king0[12];
        king0[12].cChildren[0] = king0[13];
        king0[13].cChildren[0] = king0[14];
        king0[14].cChildren[0] = king0[15];
        king0[14].cChildren[0] = king0[15];
        king0[15].cChildren[0] = king0[16];
        king0[16].cChildren[0] = king0[17];
        king0[17].cChildren[0] = king0[18];
        king0[17].cChildren[1] = king0[18];
        king0[18].cChildren[0] = king0[19];
        king0[18].cChildren[1] = king0[20];
        king0[18].cChildren[2] = king0[21];

        //King 1
        //0
        cs = new ConversationState("T_King_normal", "Black King", "I've never seen two kings so close together in chess",
            new bool[] { true, true, true }, new string[] { "Do you fear my fury?", "My mom says there's nothing wrong with it", "You lack I.M.A.G.I.N.A.T.I.O.N." });
        king1.Add(cs);
        //1
        cs = new ConversationState("T_King_normal", "Scared Black King", "A little bit, I'd rather surrender, to be honest...",
            new bool[] { true, false, false }, new string[] { "Join the others", "", "" });
        king1.Add(cs);
        //2
        cs = new ConversationState("T_King_normal", "Black King", "I don't like what you're doing with this place",
            new bool[] { true, true, false }, new string[] { "I'm sorry, it has to be done.", "And I don't like your face, ugly.", "" });
        king1.Add(cs);
        //3
        cs = new ConversationState("T_King_normal", "Black King", "I don't even have a face...",
            new bool[] { true, false, false }, new string[] { "Whatever.", "", "" });
        king1.Add(cs);
        
        king1[0].cChildren[0] = king1[1];
        king1[0].cChildren[1] = king1[2];
        king1[0].cChildren[2] = king1[2];
        king1[2].cChildren[1] = king1[3];


        //Story 0
        //0 Story.0
        cs = new ConversationState("T_Lily", "Lily", "That's it, checkmate again!",
            new bool[] { true, false, false }, new string[] { "What does that mean?", "*Say nothing*", "" });
        story0.Add(cs);
        //1 Story.0
        cs = new ConversationState("T_Lily", "Lily", "It means you can't do nothing so I win.",
            new bool[] { true, true, false }, new string[] { "Yes I can I do this, and this, and then...", "I can talk them into being my friends", "" });
        cs.cAction = "fadein";
        story0.Add(cs);
        //2 Story.0
        cs = new ConversationState("T_Lily", "Lily", "You can't do that in chess, Sam",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        cs.cAction = "fadein";
        story0.Add(cs);
        //3 Story.0
        cs = new ConversationState("T_Question", "???", "Yes you can, Sam",
            new bool[] { true, false, false }, new string[] { "Who said that?", "", "" });
        story0.Add(cs);
        //4 Story.0
        cs = new ConversationState("T_Question", "???", "I'm your Imagination, Sam",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        story0.Add(cs);
        //5 Story.0
		cs = new ConversationState("T_Imagination", "My Imagination", "And it's been a while since the last time we had some fun, don't you think?",
            new bool[] { true, true, false }, new string[] { "I didn't want to do my homework either.", "You can bet!", "" });
        cs.cImaginationChange = 50;
        story0.Add(cs);
        //6 Story.0
		cs = new ConversationState("T_Imagination", "My Imagination", "Lame. Anyway why don't we do something with this chess thing?",
            new bool[] { true, true, false }, new string[] { "But I just lost.", "What do you want me to do?", "" });
        story0.Add(cs);
        //7 Story.0
		cs = new ConversationState("T_Imagination", "My Imagination", "Okay then, let's play this chess thing",
            new bool[] { true, false, false }, new string[] { "But I just lost.", "", "" });
        story0.Add(cs);
        //8 Story.0
		cs = new ConversationState("T_Imagination", "My Imagination", "Listen dude you can do whatever you want as long as you keep me going.",
            new bool[] { true, false, false }, new string[] { "Sounds fun.", "", "" });
        story0.Add(cs);
        //9 Story.0
		cs = new ConversationState("T_Imagination", "My Imagination", "Why don't you go talk with the enemy pieces and convince them into surrendering?",
            new bool[] { true, true, false }, new string[] { "Ok", "That's dumb.", "" });
        story0.Add(cs);
        //10 Story.0
		cs = new ConversationState("T_Imagination", "My Imagination", "Trust me kid, I've been around.",
            new bool[] { true, true, false }, new string[] { "Whatever.", "If this is a trick I swear...", "" });
        story0.Add(cs);
        //Story 0 Connections
        story0[0].cChildren[0] = story0[1];
        story0[0].cChildren[1] = story0[1];
        story0[1].cChildren[0] = story0[2];
        story0[1].cChildren[1] = story0[2];
        story0[2].cChildren[0] = story0[3];
        story0[3].cChildren[0] = story0[4];
        story0[4].cChildren[0] = story0[5];
        story0[5].cChildren[0] = story0[6];
        story0[5].cChildren[1] = story0[7];
        story0[6].cChildren[0] = story0[8];
        story0[6].cChildren[1] = story0[8];
        story0[7].cChildren[0] = story0[8];
        story0[8].cChildren[0] = story0[9];
        story0[9].cChildren[1] = story0[10];
        
        //Story 1
        //0
        cs = new ConversationState("T_Lily", "Lily", "Why are you doing this, Sam?",
            new bool[] { true, false, false }, new string[] { "The voice from my head told me to do it", "", "" });
        cs.cAction = "fadeout";
        story1.Add(cs);
        //1
        cs = new ConversationState("T_Lily", "Lily", "What?",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        cs.cAction = "load_level2";
        story1.Add(cs);
        //2
		cs = new ConversationState("T_Imagination", "My Imagination", "There are more coming!",
            new bool[] { true, false, false }, new string[] { "...", "", "" });
        cs.cAction = "fadein";
        story1.Add(cs);

        story1[0].cChildren[0] = story1[1];
        story1[1].cChildren[0] = story1[2];


        //Story 2
        //0
		cs = new ConversationState("T_Imagination", "My Imagination","I believe those were the last ones",
            new bool[] { true, true, false }, new string[] { "So did we win?", "We Won!", "" });
        cs.cAction = "fadeout";
        story2.Add(cs);
        //1
		cs = new ConversationState("T_Imagination", "My Imagination", "I don't know man I've never done this before",
            new bool[] { true, false, false }, new string[] { "oh, ok...", "", "" });
        cs.cAction = "load_level3";
        story2.Add(cs);
        //2
        cs = new ConversationState("T_Imagination", "My Imagination", "Oh, it seems like there is one more. Go get him kid",
            new bool[] { true, false, false }, new string[] { "Where do they keep coming from, by the way?", "", "" });
        cs.cAction = "fadein";
        story2.Add(cs);

        story2[0].cChildren[0] = story2[1];
        story2[0].cChildren[1] = story2[1];
        story2[1].cChildren[0] = story2[2];
    }
}
