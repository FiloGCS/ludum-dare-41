﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	//PARAMETERS
	public Image ingameCurtain;
    public string DEFAULT_PLAYER_PIECE = "King";
    public int MAP_WIDTH = 20;
    public int STARTING_ENEMIES = 10;
    public int BISHOP_RANGE = 3; //Must be higher than 0
    public int ROOK_RANGE = 3; //Must be higher than 0
    public float imagination = 0;

    //PUBLIC ATTRIBUTES
    public bool bIsPaused = false;
    public GameObject[,] tiles;
    public Player player;

    //PRIVATE ATTRIBUTES
    private List<Enemy> enemies;
    private GameObject currentTalker;
    private GameObject currentListener;

    public int level = 0;

    //CONSTRUCTORS AND BASICS
    void Start () {
        //Generate Map
        SpawnTiles();
        SpawnPlayer();

        //Level 1
        SpawnLevel1Enemies();
        bIsPaused = true;
        GetComponent<ConversationController>().StartStory(0);
        level = 1;
    }
	
    void Update() {
        //print(imagination);
        //Are we on pause?
        if (bIsPaused) {
            foreach(Enemy enemy in enemies){
                enemy.nextMoveTime += Time.deltaTime;
            }
            player.TimeForNextMove += Time.deltaTime;
        }

        //Level States
        switch (level) {
            case 0:
                break;
            case 1:
                if (enemies.Count <= 0) {
                    GetComponent<ConversationController>().StartStory(1);
                }
                break;
            case 2:
                if (enemies.Count <= 0) {
                    GetComponent<ConversationController>().StartStory(2);
                }
                break;
            default:
                break;
        }
    }
    
    //SpawnTiles - Initializes and spawns the array of tiles in the scene
    void SpawnTiles() {
        //Init array of tiles
        tiles = new GameObject[MAP_WIDTH, MAP_WIDTH];
        //Spawning the array of tiles
        for (int i=0; i<MAP_WIDTH; i++) {
            for(int j=0; j<MAP_WIDTH; j++) {
                //Spawning the tile Gamebject at this position
                tiles[i, j] = (GameObject) Instantiate(Resources.Load("Tile"));
                tiles[i, j].transform.rotation = Quaternion.Euler(Vector3.zero);
                tiles[i, j].transform.position = new Vector3(i, 0, j);
                //Initializing the Tile Object
                tiles[i, j].GetComponent<Tile>().SetPosition(i, j);
                tiles[i, j].GetComponent<Tile>().Init(this);
            }
        }
    }

    //SpawnPlayer - Spawns the player GameObject
    void SpawnPlayer() {
        //Instantiate the player piece
        GameObject playerPiece = (GameObject)Instantiate(Resources.Load(DEFAULT_PLAYER_PIECE));
        playerPiece.transform.rotation = Quaternion.Euler(Vector3.zero);
        playerPiece.GetComponent<Piece>().SetPosition(7, 7);
        //Make the camera follow the player
        this.GetComponent<CameraController>().SetTarget(playerPiece);
        //Create the Player
        playerPiece.AddComponent(typeof(Player));
        player = playerPiece.GetComponent<Player>();
        player.Init(this, tiles[playerPiece.GetComponent<Piece>().x, playerPiece.GetComponent<Piece>().y]);
    }

    void SpawnLevel1Enemies() {
        enemies = new List<Enemy>();
        SpawnEnemy("Bishop", 4, 3,0);
        SpawnEnemy("King", 1, 1, 1);
        SpawnEnemy("Horse", 5, 6, 0);
        SpawnEnemy("Rook", 6, 2, 0);
    }

	public void SpawnLevel2() {
        //Spawn Level 2 enemies
        SpawnEnemy("Bishop", 4, 4, 1);
        SpawnEnemy("Pawn", 3, 6, 0);
        foreach(Enemy enemy in enemies) {
            enemy.transform.position += Vector3.up * 4;
        }
        //Change player to horse
        Destroy(player.gameObject);
        //Instantiate the player piece
        GameObject playerPiece = (GameObject)Instantiate(Resources.Load("horse"));
        playerPiece.transform.rotation = Quaternion.Euler(Vector3.zero);
        playerPiece.GetComponent<Piece>().SetPosition(7, 7);
        //Make the camera follow the player
        this.GetComponent<CameraController>().target = playerPiece;
        //Create the Player
        playerPiece.AddComponent(typeof(Player));
        player = playerPiece.GetComponent<Player>();
        player.Init(this, tiles[playerPiece.GetComponent<Piece>().x, playerPiece.GetComponent<Piece>().y]);
        currentTalker = player.gameObject;
        //Clear the marked tiles from the previous player
        foreach(GameObject tile in tiles) {
            tile.GetComponent<Tile>().SetIsPossibleForPlayer(false);
        }

        level = 2;
    }

    public void SpawnLevel3() {
        //Spawn Level 2 enemies
        SpawnEnemy("King", 1, 3, 0);
        foreach (Enemy enemy in enemies) {
            enemy.transform.position += Vector3.up * 4;
        }
        //Change player to horse
        Destroy(player.gameObject);
        //Instantiate the player piece
        GameObject playerPiece = (GameObject)Instantiate(Resources.Load("pawn"));
        playerPiece.transform.rotation = Quaternion.Euler(Vector3.zero);
        playerPiece.GetComponent<Piece>().SetPosition(6, 4);
        //Make the camera follow the player
        this.GetComponent<CameraController>().target = playerPiece;
        //Create the Player
        playerPiece.AddComponent(typeof(Player));
        player = playerPiece.GetComponent<Player>();
        player.Init(this, tiles[playerPiece.GetComponent<Piece>().x, playerPiece.GetComponent<Piece>().y]);
        currentTalker = player.gameObject;
        //Clear the marked tiles from the previous player
        foreach (GameObject tile in tiles) {
            tile.GetComponent<Tile>().SetIsPossibleForPlayer(false);
        }

        level = 3;
    }

    //Spawns a single enemy
    public void SpawnEnemy(string pieceType, int x, int y, int conversationNumber) {
        //Instantiate the enemy piece
        GameObject newEnemyPiece = (GameObject)Instantiate(Resources.Load(pieceType));
        newEnemyPiece.transform.rotation = Quaternion.Euler(Vector3.zero);
        newEnemyPiece.GetComponent<Piece>().SetPosition(x, y);
        newEnemyPiece.AddComponent(typeof(Enemy));
        Enemy newEnemy = newEnemyPiece.GetComponent<Enemy>();
        //Init the new Enemy
        newEnemy.Init(this, tiles[x,y]);
        newEnemy.conversationNumber = conversationNumber;
        //Add it to the list of enemies
        enemies.Add(newEnemy);
    }
    
    public void StartConversation(GameObject talker, GameObject listener) {
        bIsPaused = true;
        currentTalker = talker;
        currentListener = listener;
        //Get the camera close to the action!
        GetComponent<CameraController>().target = talker;
        GetComponent<CameraController>().target2 = listener;
        GetComponent<ConversationController>().StartConversation(talker, listener);
    }

    public void AddImagination(float amount) {
        imagination += amount;
    }

	public void EndConversation(string code){
        if (currentTalker!=null && currentTalker.GetComponent<Player>() != null) {
			if (currentListener != null) {
				enemies.Remove(currentListener.GetComponent<Enemy>());
			}
			tiles [player.GetComponent<Piece> ().x, player.GetComponent<Piece> ().y].GetComponent<Tile> ().SetIsOccupiedByEnemy (null);
            Destroy(currentListener);
            currentTalker.GetComponent<Player>().CommitMove();
        }
        currentTalker = null;
        currentListener = null;
        //Take the camera back
        GetComponent<CameraController>().target = player.gameObject;
        GetComponent<CameraController>().target2 = null;
        bIsPaused = false;
		if (code == "load_level2") {
			SpawnLevel2 ();
		}
		if (code == "load_level3") {
			SpawnLevel3 ();
		}
    }

    //Get a list of positions a piece can move to
    public List<int[]> GetPossibleMoves(Piece piece) {
        List<int[]> results = new List<int[]>();
        switch (piece.type) {
            case PieceType.King:
                //King normal moves
                results.Add(new int[] { piece.x + 1, piece.y });
                results.Add(new int[] { piece.x - 1, piece.y });
                results.Add(new int[] { piece.x, piece.y + 1 });
                results.Add(new int[] { piece.x, piece.y - 1 });
                results.Add(new int[] { piece.x + 1, piece.y + 1});
                results.Add(new int[] { piece.x - 1, piece.y + 1});
                results.Add(new int[] { piece.x + 1, piece.y - 1 });
                results.Add(new int[] { piece.x - 1, piece.y - 1 });
                break;

            case PieceType.Queen:
                for (int i = 1; i <= BISHOP_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x + i, piece.y + i });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x + i, piece.y + i })
                    && (tiles[piece.x + i, piece.y + i].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x + i, piece.y + i].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                for (int i = 1; i <= BISHOP_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x + i, piece.y - i });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x + i, piece.y - i })
                    && (tiles[piece.x + i, piece.y - i].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x + i, piece.y - i].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                for (int i = 1; i <= BISHOP_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x - i, piece.y + i });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x - i, piece.y + i })
                    && (tiles[piece.x - i, piece.y + i].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x - i, piece.y + i].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                for (int i = 1; i <= BISHOP_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x - i, piece.y - i });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x - i, piece.y - i })
                    && (tiles[piece.x - i, piece.y - i].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x - i, piece.y - i].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                for (int i = 1; i <= ROOK_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x + i, piece.y });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x + i, piece.y })
                    && (tiles[piece.x + i, piece.y].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x + i, piece.y].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                for (int i = 1; i <= ROOK_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x - i, piece.y });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x - i, piece.y })
                    && (tiles[piece.x - i, piece.y].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x - i, piece.y].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                for (int i = 1; i <= ROOK_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x, piece.y + i });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x, piece.y + i })
                    && (tiles[piece.x, piece.y + i].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x, piece.y + i].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                for (int i = 1; i <= ROOK_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x, piece.y - i });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x, piece.y - i })
                    && (tiles[piece.x, piece.y - i].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x, piece.y - i].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                break;
            case PieceType.Bishop:
                for (int i = 1; i <= BISHOP_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x + i, piece.y + i});
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x + i, piece.y+i})
                    && (tiles[piece.x + i, piece.y+i].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x + i, piece.y+i].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                for (int i = 1; i <= BISHOP_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x + i, piece.y - i });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x + i, piece.y - i })
                    && (tiles[piece.x + i, piece.y - i].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x + i, piece.y - i].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                for (int i = 1; i <= BISHOP_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x - i, piece.y + i });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x - i, piece.y + i })
                    && (tiles[piece.x - i, piece.y + i].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x - i, piece.y + i].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                for (int i = 1; i <= BISHOP_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x - i, piece.y - i });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x - i, piece.y - i })
                    && (tiles[piece.x - i, piece.y - i].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x - i, piece.y - i].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                break;

            case PieceType.Rook:
                for (int i = 1; i <= ROOK_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x + i, piece.y });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x + i, piece.y })
                    && (tiles[piece.x + i, piece.y].GetComponent<Tile>().enemyOccupier
                    ||  tiles[piece.x + i, piece.y].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                for (int i = 1; i <= ROOK_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x - i, piece.y });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x - i, piece.y })
                    && (tiles[piece.x - i, piece.y].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x - i, piece.y].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                for (int i = 1; i <= ROOK_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x, piece.y +i });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x, piece.y +i})
                    && (tiles[piece.x, piece.y+i].GetComponent<Tile>().enemyOccupier
                    ||  tiles[piece.x, piece.y+i].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                for (int i = 1; i <= ROOK_RANGE; i++) {
                    //Add this position to possibles
                    results.Add(new int[] { piece.x, piece.y - i });
                    //If I'm colliding with someone
                    if (CheckValidPosition(new int[] { piece.x, piece.y - i })
                    && (tiles[piece.x, piece.y - i].GetComponent<Tile>().enemyOccupier
                    || tiles[piece.x, piece.y - i].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                        //I can't go further
                        break;
                    }
                }
                break;

            case PieceType.Horse:
                results.Add(new int[] { piece.x + 2, piece.y + 1});
                results.Add(new int[] { piece.x + 2, piece.y - 1});
                results.Add(new int[] { piece.x - 2, piece.y + 1 });
                results.Add(new int[] { piece.x - 2, piece.y - 1 });
                results.Add(new int[] { piece.x + 1, piece.y + 2 });
                results.Add(new int[] { piece.x - 1, piece.y + 2 });
                results.Add(new int[] { piece.x + 1, piece.y - 2 });
                results.Add(new int[] { piece.x - 1, piece.y - 2 });
                break;

            case PieceType.Pawn:
                //Peon passive moves
                if (CheckValidPosition(new int[] { piece.x+1, piece.y})
                && !tiles[piece.x + 1, piece.y].GetComponent<Tile>().enemyOccupier
                && !tiles[piece.x + 1, piece.y].GetComponent<Tile>().bIsOccupiedByPlayer) {
                    results.Add(new int[] { piece.x + 1, piece.y });
                }
                if (CheckValidPosition(new int[] { piece.x-1, piece.y})
                && !tiles[piece.x - 1, piece.y].GetComponent<Tile>().enemyOccupier
                && !tiles[piece.x - 1, piece.y].GetComponent<Tile>().bIsOccupiedByPlayer) {
                    results.Add(new int[] { piece.x - 1, piece.y });
                }
                if (CheckValidPosition(new int[] { piece.x, piece.y+1})
                && !tiles[piece.x, piece.y + 1].GetComponent<Tile>().enemyOccupier
                && !tiles[piece.x, piece.y + 1].GetComponent<Tile>().bIsOccupiedByPlayer) {
                    results.Add(new int[] { piece.x, piece.y + 1 });
                }
                if (CheckValidPosition(new int[] { piece.x, piece.y-1})
                && !tiles[piece.x, piece.y - 1].GetComponent<Tile>().enemyOccupier
                && !tiles[piece.x, piece.y - 1].GetComponent<Tile>().bIsOccupiedByPlayer) {
                    results.Add(new int[] { piece.x, piece.y - 1 });
                }

                //Peon attacking moves
                if (CheckValidPosition(new int[] { piece.x + 1, piece.y + 1 })
                && (tiles[piece.x + 1, piece.y + 1].GetComponent<Tile>().enemyOccupier
                || tiles[piece.x + 1, piece.y + 1].GetComponent<Tile>().bIsOccupiedByPlayer)){
                    results.Add(new int[] { piece.x + 1, piece.y + 1 });
                }
                if (CheckValidPosition(new int[] { piece.x - 1, piece.y +1})
                && (tiles[piece.x - 1, piece.y + 1].GetComponent<Tile>().enemyOccupier
                || tiles[piece.x - 1, piece.y + 1].GetComponent<Tile>().bIsOccupiedByPlayer)){
                    results.Add(new int[] { piece.x - 1, piece.y + 1 });
                }
                if (CheckValidPosition(new int[] { piece.x + 1, piece.y -1})
                && (tiles[piece.x + 1, piece.y - 1].GetComponent<Tile>().enemyOccupier
                || tiles[piece.x + 1, piece.y - 1].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                    results.Add(new int[] { piece.x + 1, piece.y - 1 });
                }
                if (CheckValidPosition(new int[] { piece.x - 1, piece.y -1 })
                && (tiles[piece.x - 1, piece.y - 1].GetComponent<Tile>().enemyOccupier
                || tiles[piece.x - 1, piece.y - 1].GetComponent<Tile>().bIsOccupiedByPlayer)) {
                    results.Add(new int[] { piece.x - 1, piece.y - 1 });
                }
                break;

            default:
                break;
        }
        //Busco la lista de resultados a eliminar
        List<int[]> toBeRemoved = new List<int[]>();
        foreach(int[] result in results) {
            if(!(result[0] < MAP_WIDTH && result[0] >= 0 && result[1] < MAP_WIDTH && result[1] >= 0)) {
                toBeRemoved.Add(result);
            }
        }
        //Una vez terminado el interator los elimino todos
        foreach(int[] result in toBeRemoved) {
            results.Remove(result);
        }
        return results;
    }

    public bool CheckValidPosition(int[] position) {
        return (position[0] < MAP_WIDTH && position[0] >= 0 && position[1] < MAP_WIDTH && position[1] >= 0);
    }


    public void TogglePause() {
        bIsPaused = !bIsPaused;
    }

    public void Exit() {
        Application.Quit();
    }
    public void Restart() {
        //TODO
		//Application.LoadLevel(Application.loadedLevel);
		StartCoroutine ("AnimatedRestart");
	}

	public IEnumerator AnimatedRestart(){
		ingameCurtain.CrossFadeAlpha(1f, 0.5f, false);
		yield return new WaitForSeconds (0.5f);
		SceneManager.LoadScene("Menu");
	}
}
