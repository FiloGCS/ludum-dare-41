﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour {

    public float MOVEMENT_DURATION = 0.5f;
    public float VIBRATION_INTENSITY = 0.03f;
    public int x;
    public int y;
    public PieceType type;
    public Color color;
    public float vibration = 0;

    private Vector3 targetPosition;
    private float movementStartTime;

    void Start() {
        this.transform.position = new Vector3(x, 0, y);
        targetPosition = this.transform.position;
    }

    void Update() {
        //Interpolation t
        float t = Mathf.Max(0, Mathf.Min(1, Time.time - movementStartTime));
        this.transform.position =
            new Vector3(Mathf.SmoothStep(this.transform.position.x, targetPosition.x + Random.Range(-VIBRATION_INTENSITY, VIBRATION_INTENSITY) * vibration, t),
            Mathf.SmoothStep(this.transform.position.y, targetPosition.y, t),// + (1 - Mathf.Abs(t-0.5f))*0.1f,
            Mathf.SmoothStep(this.transform.position.z, targetPosition.z + Random.Range(-VIBRATION_INTENSITY, VIBRATION_INTENSITY) * vibration, t));
    }

    public void SetPosition(int x, int y) {
        this.x = x;
        this.y = y;
        targetPosition = new Vector3(x, 0, y);
        movementStartTime = Time.time;
    } 

    //Sets the color of the piece, modifying the material
    public void SetColor(Color color) {
        this.color = color;
        this.GetComponent<Renderer>().material = (color == Color.black) ?
            (Material)Resources.Load("Materials/M_Piece_Black") :
            (Material)Resources.Load("Materials/M_Piece_White");
    }
}
