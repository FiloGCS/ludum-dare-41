﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConversationPanel : MonoBehaviour {

    public Image avatarPanel;
    public Text namePanel;
    public Text dialoguePanel;
    public Image endPanel;
    public Image curtain;
    public Button[] answerButton = new Button[3];

    private RectTransform rt;

    //dialogue box
    private float targetPosition;
    private float currentPosition;
    //black curtain
    private float targetOpacity;
    private float currentOpacity;

	void Start () {
        rt = GetComponent<RectTransform>();
		currentPosition = -320;
        targetPosition = currentPosition;
	}
	
	void Update () {
        //Dialogue box
        currentPosition = Mathf.Lerp(currentPosition, targetPosition, 0.1f);
        rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom,currentPosition, 315);
        //Black Curtain
    }

    public void SetAvatar(string imageName) {
        string path = "Avatars/" + imageName;
        avatarPanel.sprite = Resources.Load<Sprite>(path);
    }

    public void SetName(string text) {
        namePanel.text = text;
    }

    public void SetDialogue(string text) {
        dialoguePanel.text = text;
    }

    public void SetAnswer(int i, string text) {
        if (i >= 0 && i < 3) {
            if (text != null) {
                answerButton[i].transform.GetChild(0).GetComponent<Text>().text = text;
                answerButton[i].gameObject.SetActive(true);
            } else {
                answerButton[i].gameObject.SetActive(false);
            }
        }
    }

    public void SetCurtain(bool curtainOn) {
        if (curtainOn) {
            this.curtain.CrossFadeAlpha(1f, 0.5f, false);
        } else {
            this.curtain.CrossFadeAlpha(0f, 0.5f, false);
        }
    }
    public void ShowEndPanel() {
        endPanel.gameObject.SetActive(true);
    }
    public void Show() {
        targetPosition = 40;
    }

    public void Hide() {
        targetPosition = -320;
    }
}
