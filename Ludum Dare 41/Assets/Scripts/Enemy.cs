﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float COOLDOWN;
    public float nextMoveTime = 2.0f;
    public int conversationNumber;

    public GameController gc;
    public string enemyName = "Random Enemy";

    public GameObject currentTile;
    
    public void Init(GameController gc, GameObject tile) {
        this.gc = gc;
        this.currentTile = tile;
        tile.GetComponent<Tile>().SetIsOccupiedByEnemy(this.gameObject);

        //Enemy will be black
        this.GetComponent<Piece>().SetColor(Color.black);
        //Set the cooldown
        COOLDOWN = 10f;
        //Add a random starting delay
        nextMoveTime = Time.time + Random.Range(3.0f, COOLDOWN);

    }

    void Update() {
        //Set the piece vibration
        this.GetComponent<Piece>().vibration = (!gc.bIsPaused) ? Mathf.Min(1,Mathf.Pow((COOLDOWN - (nextMoveTime - Time.time)) / COOLDOWN, 2)) : 0;

        //Enemy AI
        //Is it my time to move?
        if (nextMoveTime <= Time.time) {
            //From the possible moves
            List<int[]> enemyPotentialMoves = gc.GetPossibleMoves(this.GetComponent<Piece>());
            //TODO better strategy - Choose one at random 
            int[] selectedMove = SelectTile(enemyPotentialMoves);
            //If the chosen tile is free
            GameObject nextTile = gc.tiles[selectedMove[0], selectedMove[1]];
            if (nextTile.GetComponent<Tile>().enemyOccupier == false) {
                //Move to the new tile
                nextTile.GetComponent<Tile>().SetIsOccupiedByEnemy(this.gameObject);
                GetComponent<Piece>().SetPosition(nextTile.GetComponent<Tile>().x, nextTile.GetComponent<Tile>().y);
                //Leave the previous tile
                if (currentTile != null) currentTile.GetComponent<Tile>().SetIsOccupiedByEnemy(null);
                currentTile = nextTile;
                //Enter cooldown
                nextMoveTime = Time.time + COOLDOWN;
            }
        }
    }

    private int[] SelectTile(List<int[]> moves) {
        bool isValid = false;
        int[] move;
        while (!isValid) {
            move = moves[Random.Range(0, moves.Count - 1)];
            if (!gc.tiles[move[0], move[1]].GetComponent<Tile>().bIsOccupiedByPlayer) {
                return move;
            }
        }
        return null;
    }
}
