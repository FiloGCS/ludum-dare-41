﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CameraController : MonoBehaviour {

    public float MOVEMENT_TIGHTNESS = 0.01f;
    public float ROTATION_TIGHTNESS = 0.2f;


    private GameObject MyCamera;
    public GameObject target;
    public GameObject target2;
    public Vector3 offset;

    private Vector3 targetPosition;
    private Vector3 targetPosition2;
    private Quaternion targetRotation;
    private PostProcessingProfile profile;

    void Awake () {
        MyCamera = GameObject.Find("Main Camera");
        profile = MyCamera.GetComponent<PostProcessingBehaviour>().profile;
    }
	
	// Update is called once per frame
	void Update () {
        if (target2!= null) {
            //Baby meet me halfway
            Vector3 halfway = (target2.transform.position + Vector3.up/2.0f);
            targetPosition = halfway + offset * 0.4f;
            targetRotation = Quaternion.LookRotation(halfway - MyCamera.transform.position);
        } else {
            //Update my target position and rotation
            targetPosition = (target.transform.position+new Vector3(8,0,0))/2 + offset;
            targetRotation = Quaternion.LookRotation(new Vector3(4f, 0f, 4f) - MyCamera.transform.position);
        }
        //Move and look at my target
        MyCamera.transform.position = Vector3.Lerp(MyCamera.transform.position, targetPosition, MOVEMENT_TIGHTNESS);
        MyCamera.transform.rotation = Quaternion.Lerp(MyCamera.transform.rotation, targetRotation, ROTATION_TIGHTNESS);

        //Update DoF
        DepthOfFieldModel.Settings dofSettings = profile.depthOfField.settings;
        if (target2 != null) {
            dofSettings.focusDistance = Vector3.Magnitude(MyCamera.transform.position - target2.transform.position);
        } else {
        dofSettings.focusDistance = Vector3.Magnitude(MyCamera.transform.position - target.transform.position);
        }
        profile.depthOfField.settings = dofSettings;
    }

    public void SetTarget(GameObject newTarget) {
        target = newTarget;
        offset = MyCamera.transform.position - target.transform.position;
    }
}
