﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConversationState{
    public string cSound;
    public float cImaginationChange = 0;
    public string cAction;
    public string cAvatar;
    public string cName;
    public string cDialogue;
    public bool[] cAnswers = new bool[3];
    public string[] cAnswersText = new string[3];
    public ConversationState[] cChildren = new ConversationState[3];

    public ConversationState(string cAvatar, string cName, string cDialogue, bool[] cAnswers, string[] cAnswersText) {
        this.cAvatar = cAvatar;
        this.cName = cName;
        this.cDialogue = cDialogue;
        this.cAnswers = cAnswers;
        this.cAnswersText = cAnswersText;
    }
}
